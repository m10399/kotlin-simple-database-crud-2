package com.example.sqlitekullanimi

import android.os.Bundle
import android.util.Log
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val vt = VeriTabaniYardimcisi(this)
//        Kisilerdao().kisiEkle(vt,"Mehmet","8888",18,1.69)
//        Kisilerdao().kisiEkle(vt,"Hasan","33333",25,1.69)
//        Kisilerdao().kisiEkle(vt,"Ali","11111",23,1.69)
        //Kisilerdao().kisiGuncelle(vt,3,"Yeni Zeynep","111",100,1.22)
          //Kisilerdao().kisiSil(vt,3)
        //val sonuc = Kisilerdao().kayitKontrol(vt,"Ahmet")
        //Log.e("Kayıt Kontrol",sonuc.toString())
        val kisi = Kisilerdao().kisiGetir(vt,4)
        if (kisi !=null){
            Log.e("Tek Kişi no",(kisi.kisi_no).toString())
            Log.e("Tek Kişi ad",(kisi.kisi_ad).toString())
            Log.e("Tek Kişi tel",(kisi.kisi_tel).toString())
            Log.e("Tek Kişi yaş",(kisi.kisi_yas).toString())
            Log.e("Tek Kişi boy",(kisi.kisi_boy).toString())
        }
        val kisiliste = Kisilerdao().tumKisiler(vt)
        //val kisiliste= Kisilerdao().arama(vt,"e")
        //val kisiliste = Kisilerdao().rastgeleGetir(vt)
        for (k in kisiliste){
            Log.e("Kişi no",(k.kisi_no).toString())
            Log.e("Kişi ad",(k.kisi_ad).toString())
            Log.e("Kişi tel",(k.kisi_tel).toString())
            Log.e("Kişi yaş",(k.kisi_yas).toString())
            Log.e("Kişi boy",(k.kisi_boy).toString())
        }
    }
}